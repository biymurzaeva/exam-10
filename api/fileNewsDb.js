const fs = require('fs');
const {nanoid} = require('nanoid');
const filename = './newsDb.json';
let data = [];

module.exports = {
	init() {
		try {
			const fileContents = fs.readFileSync(filename);
			data = JSON.parse(fileContents);
		} catch (e) {
			data = [];
		}
	},
	getNews() {
		return data;
	},
	getNewsById(id) {
		return data.find(i => i.id === id);
	},
	addNew(item) {
		item.id = nanoid();
		item.date = new Date().toISOString();
		data.push(item);
		this.save();
		return item;
	},
	deletePost(id) {
		const postIndex = data.findIndex(i => i.id === id);
		data.splice(postIndex, 1);
		this.save();
		return {"delete": "Deleted"};
	},
	save() {
		fs.writeFileSync(filename, JSON.stringify(data));
	}
};