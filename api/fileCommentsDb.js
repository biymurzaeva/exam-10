const fs = require('fs');
const {nanoid} = require('nanoid');
const filename = './commentsDb.json';
const fileNews = './newsDb.json';

let data = [];
let newsData = [];

module.exports = {
	init() {
		try {
			const fileContents = fs.readFileSync(filename);
			data = JSON.parse(fileContents);
		} catch (e) {
			data = [];
		}
	},
	getComments() {
		return data;
	},
	addComment(comment) {
		try {
			const newsContents = fs.readFileSync(fileNews);
			newsData = JSON.parse(newsContents);
		} catch (e) {
			newsData = [];
		}

		let thisNewsId = false;

		newsData.forEach(d => {
			if (d.id === comment.news_id) {
				thisNewsId = true;
			}
		});

		if (thisNewsId === false){
			return {"error": "This news id not found"}
		} else if (thisNewsId === true){
			comment.id = nanoid();
			data.push(comment);
			this.save();
			return comment;
		}
	},
	deleteComment(id) {
		const itemIndex = data.findIndex(i => i.id === id);
		data.splice(itemIndex, 1);
		this.save();
		return {"delete": "Deleted"};
	},
	save() {
		fs.writeFileSync(filename, JSON.stringify(data));
	}
};