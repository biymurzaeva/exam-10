const express = require('express');
const multer = require('multer');
const path = require('path');
const config = require('../config');
const fileNewsDb = require('../fileNewsDb');
const {nanoid} = require("nanoid");

const storage = multer.diskStorage({
	destination: (req, file, cb) => {
		cb(null, config.uploadPath);

	},
	filename: (req, file, cb) => {
		cb(null, nanoid() + path.extname(file.originalname));
	}
});

const upload = multer({storage});

const router = express.Router();

router.get('/', (req, res) => {
	const news = fileNewsDb.getNews();
	res.send(news);
});

router.get('/:id', (req, res) => {
	const news  = fileNewsDb.getNewsById(req.params.id);
	if (!news) {
		return res.status(404).send({error: 'News not found'});
	}

	res.send(news);
});

router.post('/', upload.single('image'), (req, res) => {
	if (!req.body.title || !req.body.description) {
		return res.status(404).send({error: "Data not valid"});
	}

	const news = {
		title: req.body.title,
		description: req.body.description,
	};

	if (req.file) {
		news.image = req.file.filename;
	}

	const newNews = fileNewsDb.addNew(news);

	res.send(newNews);
});

router.delete('/:id', (req, res) => {
	const items = fileNewsDb.deletePost(req.params.id);
	res.send(items);
});

module.exports = router;