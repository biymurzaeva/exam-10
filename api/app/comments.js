const express = require('express');
const fileCommentsDb = require('../fileCommentsDb');

const router = express.Router();

router.get('/', (req, res) => {
	const comments = fileCommentsDb.getComments();
	res.send(comments);
});

router.post('/',(req, res) => {
	if (!req.body.comment || !req.body.news_id) {
		return res.status(404).send({error: "Data not valid"});
	}

	const comment = {
		news_id: req.body.news_id,
		author: req.body.author || "Anonymous",
		comment: req.body.comment,
	};

	const newComment = fileCommentsDb.addComment(comment);

	res.send(newComment);
});

router.delete('/:id', (req, res) => {
	const items = fileCommentsDb.deleteComment(req.params.id);
	res.send(items);
});

module.exports = router;