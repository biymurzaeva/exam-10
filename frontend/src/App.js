import Layout from "./components/UI/Layout/Layout";
import {Route, Switch} from "react-router-dom";
import News from "./containers/News/News";
import NewPost from "./containers/NewPost/NewPost";
import Post from "./containers/Post/Post";

const App = () => (
  <Layout>
    <Switch>
      <Route path="/" exact component={News}/>
      <Route path="/posts/new" component={NewPost}/>
      <Route path="/news/:id" component={Post}/>
    </Switch>
  </Layout>
);

export default App;
