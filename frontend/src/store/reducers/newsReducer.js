import {
	DELETE_POST_FAILURE,
	DELETE_POST_REQUEST,
	DELETE_POST_SUCCESS,
	FETCH_NEWS_FAILURE,
	FETCH_NEWS_REQUEST,
	FETCH_NEWS_SUCCESS,
	FETCH_POST_FAILURE,
	FETCH_POST_REQUEST,
	FETCH_POST_SUCCESS
} from "../actions/newsAction";

const initialState = {
	fetchLoading: false,
	singleLoading: false,
	deleteLoading: false,
	news: [],
	post: null,
	error: null
};

const newsReducer = (state = initialState, action) => {
	switch (action.type) {
		case FETCH_NEWS_REQUEST:
			return {...state, fetchLoading: true};
		case FETCH_NEWS_SUCCESS:
			return {...state, fetchLoading: false, news: action.payload};
		case FETCH_NEWS_FAILURE:
			return {...state, fetchLoading: false}
		case FETCH_POST_REQUEST:
			return {...state, singleLoading: true};
		case FETCH_POST_SUCCESS:
			return {...state, singleLoading: false, post: action.payload};
		case FETCH_POST_FAILURE:
			return {...state, singleLoading: false}
		case DELETE_POST_REQUEST:
			return {...state, loading: true}
		case DELETE_POST_SUCCESS:
			return {...state, loading: false}
		case DELETE_POST_FAILURE:
			return {...state, loading: false, error: action.payload}
		default:
			return state;
	}
};

export default newsReducer;