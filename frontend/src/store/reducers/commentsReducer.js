import {FETCH_COMMENTS_FAILURE, FETCH_COMMENTS_REQUEST, FETCH_COMMENTS_SUCCESS} from "../actions/commentsAction";

const initialState = {
	fetchLoading: false,
	comments: null,
};

const commentsReducer = (state = initialState, action) => {
	switch (action.type) {
		case FETCH_COMMENTS_REQUEST:
			return {...state, fetchLoading: true};
		case FETCH_COMMENTS_SUCCESS:
			return {...state, fetchLoading: false, comments: action.payload};
		case FETCH_COMMENTS_FAILURE:
			return {...state, fetchLoading: false}
		default:
			return state;
	}
};

export default commentsReducer;
