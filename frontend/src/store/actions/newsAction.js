import axios from "axios";

export const FETCH_NEWS_REQUEST = 'FETCH_NEWS_REQUEST';
export const FETCH_NEWS_SUCCESS = 'FETCH_NEWS_SUCCESS';
export const FETCH_NEWS_FAILURE = 'FETCH_NEWS_FAILURE';

export const CREATE_POST_REQUEST = 'CREATE_POST_REQUEST';
export const CREATE_POST_SUCCESS = 'CREATE_POST_SUCCESS';
export const CREATE_POST_FAILURE = 'CREATE_POST_FAILURE';

export const FETCH_POST_REQUEST = 'FETCH_POST_REQUEST';
export const FETCH_POST_SUCCESS = 'FETCH_POST_SUCCESS';
export const FETCH_POST_FAILURE = 'FETCH_POST_FAILURE';

export const DELETE_POST_REQUEST = 'DELETE_POST_REQUEST';
export const DELETE_POST_SUCCESS = 'DELETE_POST_SUCCESS';
export const DELETE_POST_FAILURE = 'DELETE_POST_FAILURE';

export const fetchNewsRequest = () => ({type: FETCH_NEWS_REQUEST});
export const fetchNewsSuccess = news => ({type: FETCH_NEWS_SUCCESS, payload: news});
export const fetchNewsFailure = () => ({type: FETCH_NEWS_FAILURE});

export const createPostRequest = () => ({type: CREATE_POST_REQUEST});
export const createPostSuccess = () => ({type: CREATE_POST_SUCCESS});
export const createPostFailure = () => ({type: CREATE_POST_FAILURE});

export const fetchPostRequest = () => ({type: FETCH_POST_REQUEST});
export const fetchPostSuccess = post => ({type: FETCH_POST_SUCCESS, payload: post});
export const fetchPostFailure = () => ({type: FETCH_POST_FAILURE});

export const deletePostRequest = () => ({type: DELETE_POST_REQUEST});
export const deletePostSuccess = () => ({type: DELETE_POST_SUCCESS});
export const deletePostFailure = error => ({type: DELETE_POST_FAILURE, payload: error});

export const fetchNews = () => {
	return async dispatch => {
		try {
			dispatch(fetchNewsRequest());
			const response = await axios.get('http://127.0.0.1:8000/news');
			dispatch(fetchNewsSuccess(response.data));
		} catch (e) {
			dispatch(fetchNewsFailure());
		}
	};
};

export const createPost = postData => {
	return async dispatch => {
		try {
			dispatch(createPostRequest());
			await axios.post(`http://127.0.0.1:8000/news`, postData);
			dispatch(createPostSuccess());
		} catch (e) {
			dispatch(createPostFailure());
			throw e;
		}
	};
};

export const fetchPost = id => {
	return async dispatch => {
		try {
			dispatch(fetchPostRequest());
			const response = await axios.get(`http://127.0.0.1:8000/news/${id}`);
			dispatch(fetchPostSuccess(response.data));
		} catch (e) {
			dispatch(fetchPostFailure());
		}
	};
};

export const deletePost = id => {
	return async (dispatch) => {
		try {
			dispatch(deletePostRequest());
			await axios.delete(`http://127.0.0.1:8000/news/${id}`);
			dispatch(deletePostSuccess());
		} catch (error) {
			dispatch(deletePostFailure(error));
		}
	}
};