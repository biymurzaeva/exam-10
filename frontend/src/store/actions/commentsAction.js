import axios from "axios";

export const FETCH_NEWS_COMMENTS_REQUEST = 'FETCH_NEWS_COMMENTS_REQUEST';
export const FETCH_NEWS_COMMENTS_SUCCESS = 'FETCH_NEWS_COMMENTS_SUCCESS';
export const FETCH_NEWS_COMMENTS_FAILURE = 'FETCH_NEWS_COMMENTS_FAILURE';

export const FETCH_COMMENTS_REQUEST = 'FETCH_COMMENTS_REQUEST';
export const FETCH_COMMENTS_SUCCESS = 'FETCH_COMMENTS_SUCCESS';
export const FETCH_COMMENTS_FAILURE = 'FETCH_COMMENTS_FAILURE';

export const DELETE_COMMENT_REQUEST = 'DELETE_COMMENT_REQUEST';
export const DELETE_COMMENT_SUCCESS = 'DELETE_COMMENT_SUCCESS';
export const DELETE_COMMENT_FAILURE = 'DELETE_COMMENT_FAILURE';

export const fetchNewsCommentsRequest = () => ({type: FETCH_NEWS_COMMENTS_REQUEST});
export const fetchNewsCommentsSuccess = () => ({type: FETCH_NEWS_COMMENTS_SUCCESS});
export const fetchNewsCommentsFailure = () => ({type: FETCH_NEWS_COMMENTS_FAILURE});

export const fetchCommentsRequest = () => ({type: FETCH_COMMENTS_REQUEST});
export const fetchCommentsSuccess = comments => ({type: FETCH_COMMENTS_SUCCESS, payload: comments});
export const fetchCommentsFailure = () => ({type: FETCH_COMMENTS_FAILURE});

export const deleteCommentRequest = () => ({type: DELETE_COMMENT_REQUEST});
export const deleteCommentSuccess = () => ({type: DELETE_COMMENT_SUCCESS});
export const deleteCommentFailure = () => ({type: DELETE_COMMENT_FAILURE});

export const fetchNewsComments = comment => {
	return async dispatch => {
		try {
			dispatch(fetchNewsCommentsRequest());
			await axios.post(`http://127.0.0.1:8000/comments`, comment);
			dispatch(fetchNewsCommentsSuccess());
		} catch (e) {
			dispatch(fetchNewsCommentsFailure());
		}
	};
};

export const fetchComments = () => {
	return async dispatch => {
		try {
			dispatch(fetchCommentsRequest());
			const response = await axios.get(`http://127.0.0.1:8000/comments`);
			dispatch(fetchCommentsSuccess(response.data));
		} catch (e) {
			dispatch(fetchCommentsFailure());
		}
	};
};

export const deleteComment = id => {
	return async (dispatch) => {
		try {
			dispatch(deleteCommentRequest());
			await axios.delete(`http://127.0.0.1:8000/comments/${id}`);
			dispatch(deleteCommentSuccess());
		} catch (e) {
			dispatch(deleteCommentFailure());
			throw e;
		}
	}
}