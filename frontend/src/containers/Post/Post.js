import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {Box, Paper, Typography} from "@material-ui/core";
import CommentForm from "../../components/CommentForm/CommentForm";
import {deleteComment, fetchComments, fetchNewsComments} from "../../store/actions/commentsAction";
import {fetchPost} from "../../store/actions/newsAction";
import Comment from "../../components/Comment/Comment";

const Post = ({match}) => {
	const dispatch = useDispatch();
	const post = useSelector(state => state.news.post);
	const comments = useSelector(state => state.comments.comments);

	useEffect( () => {
		 dispatch(fetchPost(match.params.id));
		 dispatch(fetchComments())
	}, [dispatch, match.params.id]);

	const onSubmit = async commentData => {
		await dispatch(fetchNewsComments(commentData));
		await dispatch(fetchComments());
	};

	const deleteCurrentComment = async id => {
		await dispatch(deleteComment(id));
		dispatch(fetchComments());

	};

	return (post && comments) && (
		<>
			<Paper component={Box} p={2}>
				<Typography variant="h4">{post.title}</Typography>
				<Typography variant="body1">{post.description}</Typography>
			</Paper>
			<Typography>Comments</Typography>
			{comments.map(comment => {
				if (comment.news_id === match.params.id) {
					return (
						<Comment
							key={comment.id}
							author={comment.author}
							comment={comment.comment}
							deleteComment={() => deleteCurrentComment(comment.id)}
						/>
					);
				}
			})}
			<CommentForm
				onSubmit={onSubmit}
				newsID={match.params.id}
			/>
		</>

	);
};

export default Post;