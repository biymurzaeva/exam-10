import React, {useEffect} from 'react';
import {Button, Grid, Typography} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import {Link} from "react-router-dom";
import {deletePost, fetchNews} from "../../store/actions/newsAction";
import NewsItem from "../../components/NewsItem/NewsItem";

const Products = () => {
	const dispatch = useDispatch();
	const news = useSelector(state => state.news.news);

	useEffect(() => {
		dispatch(fetchNews());
	}, [dispatch]);

	const deleteCurrentPost = async id => {
		await dispatch(deletePost(id));
		await dispatch(fetchNews());
	};

	return (
		<Grid container direction="column" spacing={2}>
			<Grid item container justifyContent="space-between" alignItems="center">
				<Grid item>
					<Typography variant="h4" >Posts</Typography>
				</Grid>
				<Grid>
					<Button color="primary" component={Link} to="posts/new">Add new post</Button>
				</Grid>
			</Grid>
			<Grid item container  direction="column"  spacing={1}>
				{news.map(n => (
					<NewsItem
						key={n.id}
						id={n.id}
						title={n.title}
						newsDate={n.date}
						image={n.image}
						onDelete={() => deleteCurrentPost(n.id)}
					/>
				))}
			</Grid>
		</Grid>
	);
};

export default Products;