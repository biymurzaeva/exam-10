import React from 'react';
import {useDispatch} from "react-redux";
import {Typography} from "@material-ui/core";
import PostForm from "../../components/PostForm/PostForm";
import {createPost} from "../../store/actions/newsAction";

const NewPost = ({history}) => {
	const dispatch = useDispatch();

	const onSubmit = async postData => {
		await dispatch(createPost(postData));
		history.replace('/');
	};

	return (
		<>
			<Typography variant="h4">Add new post</Typography>
			<PostForm
				onSubmit={onSubmit}
			/>
		</>
	);
};

export default NewPost;