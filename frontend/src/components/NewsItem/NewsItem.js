import React from 'react';
import {Link} from "react-router-dom";
import {
	Button,
	Card,
	CardActions,
	CardContent,
	CardHeader,
	CardMedia,
	Grid,
	makeStyles,
	Typography
} from "@material-ui/core";

import {apiURL} from "../../config";

const useStyles = makeStyles({
	media: {
		height: 0,
		paddingTop: '56.25%'
	}
});

const ProductItem = ({title, id, newsDate, image, onDelete}) => {
	const classes = useStyles();

	const date = new Date(`${newsDate}`);
	let year = date.getFullYear();
	let month = date.getMonth()+1;
	let dt = date.getDate();
	let hour = date.getHours();
	let minutes = date.getMinutes();

	if (dt < 10) {
		dt = '0' + dt;
	}
	if (month < 10) {
		month = '0' + month;
	}

	let cardImage = null;

	let cardPost = (
		<Grid item xs>
			<Card>
				<CardHeader title={title}/>
				<CardContent>
					<Typography variant={"subtitle1"}>
						{
							dt + '.' + month + '.' + year + ' ' + hour + ':'  + minutes
						}
					</Typography>
				</CardContent>
				<CardActions>
					<Link to={'/news/' + id}>
						Read full post >>>
					</Link>
					<Button onClick={e => onDelete(e.target)}>Delete</Button>
				</CardActions>
			</Card>
		</Grid>
	);

	if (image) {

		cardImage = apiURL + '/uploads/' + image;
		cardPost = (
			<Grid item xs>
				<Card>
					<CardHeader title={title}/>
					<CardMedia
						image={cardImage}
						title={title}
						className={classes.media}
					/>
					<CardContent classes={classes.card}>
						<Typography variant={"subtitle1"}>
							{
								dt + '.' + month + '.' + year + ' ' + hour + ':'  + minutes
							}
						</Typography>
					</CardContent>
					<CardActions>
						<Link to={'/news/' + id}>
							Read full post >>>
						</Link>
						<Button onClick={onDelete}>Delete</Button>
					</CardActions>
				</Card>
			</Grid>
		)
	}

	return (
		<>
			{cardPost}
		</>
	);
};

export default ProductItem;
