import React from 'react';
import {Box, Button, Paper, Typography} from "@material-ui/core";

const Comment = props => {
	return (
		<Paper component={Box} p={2} m={3}>
			<Typography variant="h4">{props.author}</Typography>
			<Typography variant="body1">{props.comment}</Typography>
			<Button onClick={props.deleteComment}>Delete comment</Button>
		</Paper>
	);
};

export default Comment;