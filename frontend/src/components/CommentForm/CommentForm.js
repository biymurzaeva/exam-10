import {Button, Grid, makeStyles, TextField} from "@material-ui/core";
import {useState} from "react";

const useStyles = makeStyles(theme => ({
	root: {
		marginTop: theme.spacing(2)
	},
}));

const CommentForm = ({onSubmit, newsID}) => {
	const classes = useStyles();

	const [state, setState] = useState({
		news_id: newsID,
		author: "",
		comment: "",
	});

	const submitFormHandler = e => {
		e.preventDefault();
		const formData = new FormData();
		Object.keys(state).forEach(key => {
			formData.append(key, state[key]);
		});

		onSubmit(formData);
	};

	const inputChangeHandler = e => {
		const name = e.target.name;
		const value = e.target.value;
		setState(prevState => {
			return {...prevState, [name]: value};
		});
	};

	return (
		<Grid
			container
			direction="column"
			spacing={2}
			component="form"
			className={classes.root}
			autoComplete="off"
			onSubmit={submitFormHandler}
		>
			<Grid item xs>
				<TextField
					fullWidth
					variant="outlined"
					name="author"
					label="Author"
					value={state.author}
					onChange={inputChangeHandler}
				/>
			</Grid>
			<Grid item xs>
				<TextField
					required
					fullWidth
					multiline
					rows={3}
					variant="outlined"
					name="comment"
					label="Comment"
					value={state.comment}
					onChange={inputChangeHandler}
				/>
			</Grid>
			<Grid item xs>
				<Button type="submit" color="primary" variant="contained">Add</Button>
			</Grid>
		</Grid>
	);
};

export default CommentForm;