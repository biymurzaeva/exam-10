import {Button, Grid, makeStyles, TextField} from "@material-ui/core";
import {useState} from "react";
import FileInput from "../UI/FileInput/FileInput";

const useStyles = makeStyles(theme => ({
	root: {
		marginTop: theme.spacing(2)
	},
}));

const PostForm = ({onSubmit}) => {
	const classes = useStyles();

	const [state, setState] = useState({
		title: "",
		description: "",
		image: null,
	});

	const submitFormHandler = e => {
		e.preventDefault();
		const formData = new FormData();
		Object.keys(state).forEach(key => {
			formData.append(key, state[key]);
		});

		onSubmit(formData);
	};

	const inputChangeHandler = e => {
		const name = e.target.name;
		const value = e.target.value;
		setState(prevState => {
			return {...prevState, [name]: value};
		});
	};

	const fileChangeHandler = e  => {
		const name = e.target.name;
		const file = e.target.files[0];
		setState(prevState => ({
			...prevState,
			[name]: file
		}));
	};

	return (
		<Grid
			container
			direction="column"
			spacing={2}
			component="form"
			className={classes.root}
			autoComplete="off"
			onSubmit={submitFormHandler}
		>
			<Grid item xs>
				<TextField
					fullWidth
					variant="outlined"
					name="title"
					label="Title"
					value={state.title}
					onChange={inputChangeHandler}
					required
				/>
			</Grid>
			<Grid item xs>
				<TextField
					required
					fullWidth
					multiline
					rows={3}
					variant="outlined"
					name="description"
					label="Content"
					value={state.description}
					onChange={inputChangeHandler}
				/>
			</Grid>
			<Grid item xs>
				<FileInput
					label="Image"
					name="image"
					onChange={fileChangeHandler}
				/>
			</Grid>
			<Grid item xs>
				<Button type="submit" color="primary" variant="contained">Create</Button>
			</Grid>
		</Grid>
	);
};

export default PostForm;